<%@ include file="header.jsp"%>
<body>
	<table class="table table-hover" id="sTable">
		<thead>
			<tr>
				<th>#</th>
				<th>ID</th>
				<th>CTime</th>
			</tr>
		</thead>
		<tbody> </tbody>
	</table>
</body>
<script>
	$.ajax({
		type : "GET",
		url : "api/sessions",
		datatype : "json",
		contentType : "application/json; charset=utf-8",
		cache : false,
		success : function(resp) {
			var rows = "";
			for(var i = 0; i < resp.sessions.length; i++) {
				rows += "<tr>";
				rows += "<td>" + i + "</td>";
				rows += "<td>" + resp.sessions[i].id + "</td>";
				rows += "<td>" + resp.sessions[i].ctime + "</td>";
				rows += "</tr>";
			}
			//console.log(rows);
			$('#sTable').find('tbody').append(rows);
		}
	});
</script>

<%@ include file="footer.jsp"%>
