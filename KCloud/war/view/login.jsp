<%@ include file="header.jsp"%>
<body>
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<strong>Sign in </strong>
				</h3>
			</div>
			<div class="panel-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Account / Email</label> 
						<input type="email" id="email" name="email" class="form-control" style="border-radius: 0px" id="exampleInputEmail1" placeholder="Enter Email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password 
						<a href="login/forget">(forgot password)</a></label> 
						<input type="password" id="password" name="password" class="form-control" style="border-radius: 0px" id="exampleInputPassword1" placeholder="Password">
					</div>
					<button id="login_btn" class="btn bt-sm btn-default">Sign in</button>
				</form>
			</div>
		</div>
	</div>

	<!-- Authentication Error Dialog -->
	<div class="modal fade" id="failedDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Error</h4>
				</div>
				<div class="modal-body">Authentication Error or No such user</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Confirm</button>
				</div>
			</div>
		</div>
	</div>

</body>

<script>
	$("#login_btn").submit(function() {
		return false;
	});

	$("#login_btn").click(function() {
		var postData = {
			"email" : $("#email").val(),
			"password" : $("#password").val()
		};

		$.ajax({
			type : 'POST',
			url : "/api/user/login",
			contentType : "application/json",
			dataType : "json",
			cache : false,
			data : JSON.stringify(postData),
			success : function(resp) {
				console.log(resp);
				$.cookie("email", resp.email, {path:'/'});
				document.location.href = "/";
			},
			error : function() {
				$('#failedDialog').modal();
			}
		});
		return false;
	});
</script>

<%@ include file="footer.jsp"%>
