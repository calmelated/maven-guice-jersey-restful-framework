<%@ include file="header.jsp"%>
<body>
	<table class="table table-hover" id="userTable">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Email</th>
				<th>Password</th>
			</tr>
		</thead>
		<tbody> </tbody>
	</table>
</body>
<script>
	$.ajax({
		type : "GET",
		url : "api/user?id=${it.id}",
		datatype : "json",
		contentType : "application/json; charset=utf-8",
		cache : false,
		success : function(resp) {
			var rows = "";
			for(var i = 0; i < resp.users.length; i++) {
				rows += "<tr onclick=location.href='user/update?id=" + resp.users[i].id + "'>";
				rows += "<td>" + i + "</td>";
				rows += "<td>" + resp.users[i].name + "</td>";
				rows += "<td>" + resp.users[i].email + "</td>";
				rows += "<td>" + resp.users[i].password + "</td>";
				rows += "</tr>";
			}
			//console.log(rows);
			$('#userTable').find('tbody').append(rows);
		}
	});
</script>

<%@ include file="footer.jsp"%>
