<%@ include file="header.jsp"%>
<script>
    $.ajax({
        type:'POST',
        url:"/api/user/logout",
        cache: false,            
        success: function(resp) {
            $.removeCookie("email", {path:'/'});
		    document.location.href = "/";
        },
        error: function() {
        	alert("Fail to logout !!");
		    document.location.href = "/";
        }
    });
</script>
<%@ include file="footer.jsp"%>
