<%@ include file="header.jsp"%>
<body>
	<h1>Test File Upload</h1>
	<form enctype="multipart/form-data" id="uploadForm" method="post">
		<input id="file_input" name="file" type="file">
	</form>
	<div id="avator_path" style="display: none;"></div>
	<button id="uploadFileButton">Upload</button>
</body>
<script>
	$(function() {
		$("#uploadFileButton").click(function() {
			$("#uploadForm").attr("action", "http://127.0.0.1:8888/files/upload");
			$("#uploadForm").submit(); 
		});
		$("#uploadForm").ajaxForm({
			url : $("uploadForm").attr("action"),
			beforeSubmit : showRequest,
			type : "post",
			dataType : "json",
			success : uploadCallback
		});
	});
	function showRequest(formData, jqForm, options) {
		console.log("Form element's ID : " + $(jqForm).attr("id"));
		return true;
	}
	function uploadCallback(responseJson, statusText, xhr, $form) {
		if (statusText == "success" && responseJson && responseJson.statuscode == 200) {
			console.log("Upload succeeded!!");
			$("#avator_path").html(responseJson.path).show();
		} else {
			console.log("Upload failed!!");
		}
	}
</script>
<%@ include file="footer.jsp"%>
