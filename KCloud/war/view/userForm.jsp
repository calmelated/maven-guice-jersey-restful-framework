<%@ include file="header.jsp"%>
<body>
	<form class="form-horizontal" role="form" id="signup_form" method="post">
		<div class="form-group">
			<label class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
				<input type="email" class="form-control" name="email" placeholder="Email / Account" value="${it.email}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="name" placeholder="Username" value="${it.name}">
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" name="password" placeholder="Password" value="${it.password}">
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-sm-2 control-label"> </label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-link" id="deleteBtn">Remove</button>
			</div>
		</div>
	</form>
</body>

<script>
    $(function() {
        var email = $.cookie("email"); 
        if ("${it.id}" == "") {
	        $("#deleteBtn").hide();
	 	}
        $("#deleteBtn").click(function() {
            $.ajax({
                type : "DELETE",
                url : "/api/user/delete?id=${it.id}",
                success : function(data) {
                    document.location.href = "/user";
                }
            });
        });
        $("#signup_form").submit(function() {
            var url = "/api/user/new";
            var ptype = "POST";
            if ("${it.id}" != "") {
                url = "/api/user/update?id=${it.id}";
                ptype = "PUT";
            }
            $.ajax({
                type : ptype,
                url : url,
                data : $("#signup_form").serialize(), 
                success : function(data) {
                    console.log(data); 
                    document.location.href = "/user";
                }
            });
            return false; // avoid to execute the actual submit of the form.
        });
    });
</script>

<%@ include file="footer.jsp"%>
