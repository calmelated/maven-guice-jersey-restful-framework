package com.ksmt.cloud;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.servlet.RequestScoped;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class WebModule extends ServletModule {

    @Override
    protected void configureServlets(){
        bind(CommonController.class).in(RequestScoped.class);
        
        Map<String, String> parameters = new HashMap<String, String>(); 
        parameters.put("com.sun.jersey.config.property.packages", "com.ksmt.cloud");
        serve("/*").with(GuiceContainer.class, parameters);
    }
    
}
