package com.ksmt.cloud;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.view.Viewable;

@Path("/file")
public class Files {
	private final Logger log = Logger.getLogger(this.getClass().getName());

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response index(){
        return Response.ok(new Viewable("/view/upfile.jsp")).build();        
    }	
	
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_HTML)
	public String fileUpload(@Context HttpServletRequest request) throws JSONException {
		String filePath = "js/";
		String fileName = "";
		JSONObject jsonOutput = new JSONObject();
		List<String> errorList = new ArrayList<String>();

		if (ServletFileUpload.isMultipartContent(request)) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setHeaderEncoding("utf-8");
			try {
				List<FileItem> items = upload.parseRequest(request);
				Iterator<FileItem> iter = items.iterator();

				while (iter.hasNext()) {
					FileItem item = iter.next();
					if (!item.isFormField() && item.getSize() > 0) {
						fileName = item.getName();
						item.write(new File(filePath + fileName));
					} else {
						errorList.add("Upload item was not correct");
					}
				}
			} catch (FileUploadException e) {
				errorList.add("Parse request error : " + e.getMessage());
			} catch (Exception e) {
				errorList.add("Have exception : " + e.getMessage());
			}
		} else {
			errorList.add("Upload item was not correct");
		}
		if (errorList.size() != 0) {
			jsonOutput.put("statuscode", 500).put("msg", errorList.toString());
		} else {
			jsonOutput.put("statuscode", 200).put("path", filePath + fileName);
		}
		return jsonOutput.toString();
	}
}