package com.ksmt.cloud;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.ksmt.cloud.jdo.PMF;
import com.ksmt.cloud.jdo.User;
import com.ksmt.cloud.jdo.UserDAO;
import com.sun.jersey.api.view.Viewable;

@Path("/user")
public class UserView {
	private final Logger logger = SysUtils.getLogger(getClass().getName(), Level.ALL);
	
	@GET
    @Produces(MediaType.TEXT_HTML)
	public Response userInfo(@Context HttpServletRequest req,
			@QueryParam("id") 	 Long	id, 
			@QueryParam("email") String email) throws JSONException 
	{			
		logger.fine(Dbg._TAG_() + "id: " + id + ", email:" + email);
		Map<String, Object> userInfo = new HashMap<String, Object>();
		if(id != null) {
	    	userInfo.put("id", id);
			return Response.ok(new Viewable("/view/userInfo.jsp",userInfo)).build();
		} else if(email != null) {
	    	userInfo.put("id", UserDAO.getUser(email).getId());
			return Response.ok(new Viewable("/view/userInfo.jsp",userInfo)).build();
		} else {
			return Response.ok(new Viewable("/view/userInfo.jsp")).build();
		}
	}	
	
	@GET
	@Path("/new")
    @Produces(MediaType.TEXT_HTML)
	public Response userForm(@Context HttpServletRequest req) throws JSONException {
		return Response.ok(new Viewable("/view/userForm.jsp")).build();
	}
	
	@GET
	@Path("/update")
    @Produces(MediaType.TEXT_HTML)
	public Response getUserCfg(@Context HttpServletRequest req, 
			@QueryParam("id") 	 Long	id, 
			@QueryParam("email") String email) throws JSONException 
	{
		logger.fine(Dbg._TAG_() + "id: " + id + ", email:" + email);
		User user = null;
		if(id != null) {
			user = UserDAO.getUser(id);
		} else if(email != null) {
			user = UserDAO.getUser(email);
		}		
		
	    HttpSession session= req.getSession(true);
	    String sessionID = session.getId();
	    if(user != null) {
	    	Map<String, Object> userInfo = new HashMap<String, Object>();
	    	userInfo.put("id", user.getId());
	    	userInfo.put("email", user.getEmail());
	    	userInfo.put("name", user.getName());
	    	userInfo.put("password", user.getPassword());
	    	return Response.ok(new Viewable("/view/userForm.jsp", userInfo)).build();
	    } else {
		    return Response.status(404)
		    	    .type(MediaType.TEXT_PLAIN)
		    	    .entity("User not found").build();
	    }		
	}
	
	@GET
	@Path("/login")
    @Produces(MediaType.TEXT_HTML)
	public Response login(@Context HttpServletRequest req) throws JSONException {			
		return Response.ok(new Viewable("/view/login.jsp")).build();
	}	
	
	@GET
	@Path("/logout")
    @Produces(MediaType.TEXT_HTML)
	public Response logout(@Context HttpServletRequest req) throws JSONException {			
		return Response.ok(new Viewable("/view/logout.jsp")).build();
	}		
	
	@GET
	@Path("/forget")
    @Produces(MediaType.TEXT_HTML)
	public Response forget(@Context HttpServletRequest req) throws JSONException {			
		return Response.ok(new Viewable("/view/forget.jsp")).build();
	}		
}
