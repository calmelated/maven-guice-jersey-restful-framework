package com.ksmt.cloud;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.inject.servlet.GuiceFilter;

public class GAEGuiceFilter extends GuiceFilter {
    private static final Pattern p = Pattern.compile("/_ah/.*|/js|/image|/css");

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        String sessionId = req.getSession(true).getId();
        if(Session.check(sessionId)) {
        	Session.set(sessionId);
        } else {
        	HttpServletResponse resp = (HttpServletResponse) response;
        	resp.sendRedirect("/user/login");
        }

        if (p.matcher(req.getRequestURI()).matches() && !req.getRequestURI().equals("/_ah/warmup")) {
            chain.doFilter(request, response);
            return ;
        }
        super.doFilter(request, response, chain);
    }	
	
}
