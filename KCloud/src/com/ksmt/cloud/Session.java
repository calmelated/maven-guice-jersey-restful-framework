package com.ksmt.cloud;

import java.util.HashMap;
import java.util.Map;

public class Session {
	private static Map<String, Long> stable = new HashMap<String, Long>();
	private static long expire = 30 * 1000000; // default: 30sec

	public static final void setExpire(long expireTime) {
		expire = expireTime;
	}

	public static final Map<String, Long> getAll() {
		return stable;
	}

	public static final void clear(String sessionId) {
		stable.remove(sessionId);
	}
	
	public static final void set(String sessionId) {
		long curTime = System.currentTimeMillis();
		stable.put(sessionId, curTime);
	}

	public static final boolean check(String sessionId) {
		long curTime = System.currentTimeMillis();
		if(stable.containsKey(sessionId)) {
			long lastTime = stable.get(sessionId);
			if((curTime - lastTime) < expire) {
				return true;
			} 
		}
		return false;
	} 
}
