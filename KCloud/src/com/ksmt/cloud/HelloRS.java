package com.ksmt.cloud;

import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import sun.util.logging.resources.logging;

import com.google.api.server.spi.config.DefaultValue;

@Path("/hello")
public class HelloRS {
	private final Logger log = Logger.getLogger(this.getClass().getName());

	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	@Context ServletContext context;
	
	@GET
	public Response sayHelloWorld() {
	    return Response
	    .ok("Hello world")
	    .type(MediaType.TEXT_PLAIN).build();
	}
	
//	@GET
//	public String sayHelloWorld1() {
//		return "Hello World";
//	}	
		
	@GET
	@Path("/notfound")
	public Response sayNotFound() {
	    return Response
	    .status(404)
	    .type(MediaType.TEXT_PLAIN)
	    .entity("resource not found").build();
	}	
	
	@GET
	@Path("/{name}")
	public String sayHello3(@PathParam("name") String name, @QueryParam("count") int count) {
		count = (count == 0) ? 1 : count;

		log.info("say hello3, name: " + name + ", count: " + count);
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < count; i++) {
			sBuffer.append("Say Hello3, " + name + "\n");
		}
		return sBuffer.toString();
	}
	
	@GET
	@Path("/{name: [a-zA-Z][0-9]*}")
	public String sayHello1(@PathParam("name") String name) {
		return "say Hello1, " + name;
	}
	
	
	@GET
	@Path("/html")   
	@Produces({"text/html", "text/plain"})
	public String sayHtmlHello() {
	     return "<h1>hello</h1>"; 
	}
	
	@POST
	@Consumes("text/plain")
	@Path("/echo")
	public String sayHelloEcho() {
	    return "echo test";      
	}	
	
	@Path("/sub")
	public Class sayHelloToMySelf() {
		return SubResource.class;
	}
	
	public static class SubResource {
		@GET
		public String get() {
			return "Hello from sub-resource";
		}
		
		@GET
		@Path("/{name}")
		public String sayHello(@PathParam("name") String name) {
			return "Say Sub-Hello, " + name;
		}
		
	}
	
}