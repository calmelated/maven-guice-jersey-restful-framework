package com.ksmt.cloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class SysUtils{
	public final static Logger getLogger(String className, Level level) {
		Logger log = Logger.getLogger(className);
		log.setLevel(level);
		return log;
	} 
	
	public static JSONObject parseJSONObj(InputStream is) throws JSONException {
		StringBuffer buffer = null;
		if (is != null) {
			buffer = new StringBuffer();
			try {
				int ch;
				Reader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				while ((ch = in.read()) > -1) {
					buffer.append((char) ch);
				}
				in.close();
			} catch (IOException e) {
				return null;
			}
		} else {
			return null;
		}
		return new JSONObject(buffer.toString());		
	}
}
