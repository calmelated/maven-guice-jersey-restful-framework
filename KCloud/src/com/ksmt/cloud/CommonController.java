package com.ksmt.cloud;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.ksmt.cloud.jdo.User;
import com.sun.jersey.api.view.Viewable;

@Path("/")
public class CommonController {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response index(){
        return Response.ok(new Viewable("/view/index.jsp")).build();        
    }
    
    @GET
    @Path("user/{userId}/{userName}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getUser(@PathParam("userId") String userId, @PathParam("userName") String userName){
        return "User ID: " + userId + ", user name: " + userName;
    }
    
    @GET
    @Path("jsp")
    @Produces(MediaType.TEXT_HTML)
    public Response getJsp(){
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("msg", "Hello World!");
        return Response.ok(new Viewable("/view/showJsp.jsp", model)).build();
    }
    
    @GET
    @Path("agent")
    @Produces(MediaType.APPLICATION_JSON)
    public String getHttpHeaders(@Context HttpHeaders headers) throws JSONException{
        String userAgent = headers.getRequestHeader("User-Agent").toString();
        return new JSONObject().put("statuscode",200).put("userAgent", userAgent).toString();
    }    
    
    @GET
    @Path("session")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSession(@Context HttpServletRequest req) throws JSONException{
        HttpSession session= req.getSession(true);
        String sessionID = session.getId();
        return new JSONObject().put("statuscode",200).put("sessionId", sessionID).toString();
    }
    
	@GET
	@Path("sessions")
    @Produces(MediaType.TEXT_HTML)
	public Response getSessions(@Context HttpServletRequest req) throws JSONException {
		return Response.ok(new Viewable("/view/sessions.jsp")).build();
	}
         
    @GET
    @Path("server")
    @Produces(MediaType.APPLICATION_JSON)
    public String getHttpRequest(@Context ServletContext context) throws JSONException{
        String serverInfo = context.getServerInfo();
        return new JSONObject().put("statuscode",200).put("serverInfo", serverInfo).toString();
    }    
}
