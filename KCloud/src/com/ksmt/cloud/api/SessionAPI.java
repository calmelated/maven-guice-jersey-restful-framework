package com.ksmt.cloud.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

import com.ksmt.cloud.Dbg;
import com.ksmt.cloud.Session;
import com.ksmt.cloud.SysUtils;
import com.ksmt.cloud.jdo.User;
import com.ksmt.cloud.jdo.UserDAO;

@Path("/api/user")
public class SessionAPI {
	private final Logger logger = SysUtils.getLogger(getClass().getName(), Level.ALL);

	@POST
	@Path("/new")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String newUser(@Context HttpServletRequest req,
			@FormParam("email") 	String email,
			@FormParam("name")		String name,
			@FormParam("password") 	String password) throws JSONException 
	{
		logger.fine(Dbg._TAG_() + "name: " + name + ", email:" + email + ", password:" + password);
		
	    String sessionId = req.getSession(true).getId();
	    UserDAO.saveUser(new User(email, name, password, sessionId));
	    return new JSONObject().put("statuscode", 200).toString();
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String modifyUserCfg(@Context HttpServletRequest req,
			@QueryParam("id")		Long   id,
			@FormParam("email") 	String email,
			@FormParam("name")		String name,
			@FormParam("password") 	String password) throws JSONException 
	{
		logger.fine(Dbg._TAG_() + "id:" + id + ", name: " + name + ", email:" + email + ", password:" + password);
		
	    HttpSession session= req.getSession(true);
	    String sessionId = session.getId();
	    User user = UserDAO.getUser(id);
	    if(user != null) {
	    	user.setEmail(email);
	    	user.setName(name);
	    	user.setPassword(password);
	    	user.setSessionId(sessionId);
	    	UserDAO.saveUser(user);
	    	return new JSONObject().put("statuscode", 200).toString();
	    } else {
	    	return new JSONObject().put("statuscode", 404).toString();
	    }
	}	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllUsers(@Context HttpServletRequest req,
			@QueryParam("id") 	 Long	id,
			@QueryParam("email") String email) throws JSONException 
	{
		logger.fine(Dbg._TAG_() + "id:" + id + ", email:" + email);
		
 		List<User> users = null; 
		if(id != null) {
			User user = UserDAO.getUser(id);
			if(user != null) {
				users = new LinkedList<User>();
				users.add(user);
			} else {
				return new JSONObject().put("statuscode", 404).toString();
			}
		} else if(email != null) {
			User user = UserDAO.getUser(email);
			if(user != null) {
				users = new LinkedList<User>();
				users.add(user);
			} else {
				return new JSONObject().put("statuscode", 404).toString();
			}			
		} else { // list all
 			users = UserDAO.getAllUsers();
		}

 		List<Map<String, String>> usersMap = new LinkedList<Map<String,String>>(); 
		JSONObject json = new JSONObject();
        for(User user: users) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", user.getId().toString());
			map.put("email", user.getEmail());
			map.put("name", user.getName());
			map.put("password", user.getPassword());
			usersMap.add(map);
        }
		json.put("statuscode", 200);		
        json.put("users", usersMap);
        return json.toString();        
	}	
	

	@DELETE
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteUser(@Context HttpServletRequest req,
			@QueryParam("id") 	 Long	id,
			@QueryParam("email") String email) throws JSONException 
	{			
		logger.fine(Dbg._TAG_() + "id:" + id + ", email:" + email);
		
		boolean result = false;
		User user = null;
		if(id != null) {
			user = UserDAO.getUser(id);
		} else if(email != null) {
			user = UserDAO.getUser(email);
		} 
		if(user != null) {
			UserDAO.deleteUser(user);
			result = true;
		}
		
		if(result) { // delete okay
			return new JSONObject().put("statuscode", 200).toString();
		} else  {
			return new JSONObject().put("statuscode", 404).toString();
		}
	}		
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String login(@Context HttpServletRequest req, InputStream is) throws JSONException {
		JSONObject requestBodyJson = SysUtils.parseJSONObj(is); 
		String email = requestBodyJson.getString("email");
		String password = requestBodyJson.getString("password");
		logger.fine(Dbg._TAG_() + "email:" + email + ", password:" + password);
		
		User user = UserDAO.getUser(email);
		if(user != null && user.getPassword().equals(password)) {
		    Session.set(req.getSession(true).getId());
			return new JSONObject().put("statuscode", 200)
					.put("email", email)
					.put("password", password)
					.toString();
		} else {
			return Response.status(404).type(MediaType.APPLICATION_JSON).build().toString();
		}
	}	
	
	@POST
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON)
    public String logout(@Context HttpServletRequest req) {
        Session.clear(req.getSession(true).getId());
        return new JSONObject().put("statuscode", 200).toString();
	}	
	
	@GET
	@Path("/sessions")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSessions(@Context HttpServletRequest req) throws JSONException {
 		List<User> users = null; 

 		List<Map<String, String>> usersMap = new LinkedList<Map<String,String>>(); 
		JSONObject json = new JSONObject();
        for(User user: users) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", user.getId().toString());
			map.put("email", user.getEmail());
			map.put("name", user.getName());
			map.put("password", user.getPassword());
			usersMap.add(map);
        }
		json.put("statuscode", 200);		
        json.put("users", usersMap);
        return json.toString();        
	}		
}
