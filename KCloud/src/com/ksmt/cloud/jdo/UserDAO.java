package com.ksmt.cloud.jdo;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.google.appengine.api.search.query.QueryParser.query_return;

public abstract class UserDAO {
	private static final Logger log = Logger.getLogger(UserDAO.class.getName());

	public static boolean saveUser(User user){
		boolean result = false;
		PersistenceManager pm = null;
		try {
			pm = PMF.get().getPersistenceManager();
			pm.makePersistent(user);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(pm != null) {
				pm.close();
			}
		}
		return result;		
	} 

	public static User getUser(Long id) {
		PersistenceManager pm = null;
		try {
			pm = PMF.get().getPersistenceManager();
			return (User) pm.getObjectById(User.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(pm != null) {
				pm.close();
			}
		}
		return null;			
	}	
	
	public static User getUser(String email) {
		PersistenceManager pm = null;
		List<User> result = null; 
		User user = null;
		try {
			pm = PMF.get().getPersistenceManager();
			result = (List<User>)pm.newQuery("SELECT FROM " + User.class.getName() + " WHERE email == '" + email + "' ").execute();
			if(result != null && result.size() > 0) {
				user = (User) result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(pm != null) {
				pm.close();
			}
		}
		return user;			
	}
	
	public static boolean deleteUser(User user) {
		boolean result = false;
		PersistenceManager pm = null;
		try {
			pm = PMF.get().getPersistenceManager();
			User savedUser = (User) pm.getObjectById(User.class, user.getId());			
			pm.deletePersistent(savedUser);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(pm != null) {
				pm.close();
			}
		}
		return result;			
	}
	
	public static List<User> getAllUsers(){
		PersistenceManager pm = null;
		List<User> result = null; 
		try {
			pm = PMF.get().getPersistenceManager();
			result = (List<User>) pm.newQuery("SELECT FROM " + User.class.getName()).execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(pm != null) {
				pm.close();
			}
		}
		return result;			
	} 
	
	
}
